#!/usr/bin/env -S csi -script
(import (chicken string)
        shell)

(define devices
  (capture (bluetoothctl devices Connected)))

(display
  (string-append
    (if (eqv? devices #!eof)
        (if (substring-index "Powered: yes"
                             (capture (bluetoothctl show)))
            "󰂯 Disconnected"
            "󰂲 Off")
        (string-append "󰂱 Connected to "
                       (string-intersperse
                         (map
                           (lambda (dev)
                             (string-intersperse
                               (cdr (cdr (string-split dev)))
                               " "))
                           (string-split devices "\n"))
                         ", ")))
    "\n"))

